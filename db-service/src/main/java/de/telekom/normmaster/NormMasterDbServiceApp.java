package de.telekom.normmaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author stpereve
 */
@SpringBootApplication
public class NormMasterDbServiceApp extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(NormMasterDbServiceApp.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(NormMasterDbServiceApp.class, args);
    }
}
