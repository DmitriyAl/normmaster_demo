package de.telekom.normmaster.service;

import de.telekom.normmaster.dto.UserDto;
import de.telekom.normmaster.entity.User;
import de.telekom.normmaster.repository.UserRepository;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Mapper mapper;

    public List<UserDto> getUsers() {
        List<User> cawfUsers = (List<User>) userRepository.findAll();
        List<UserDto> cawfUserDtos = new ArrayList<>();
        for(User cawfUser : cawfUsers){
            cawfUserDtos.add(mapper.map(cawfUser, UserDto.class));
        }
        return cawfUserDtos;
    }

    public UserDto getUserById(int id) {
        return mapper.map(userRepository.findById(id), UserDto.class);
    }

    public UserDto getUserByLoginId(String id) {
        return mapper.map(userRepository.findByLoginId(id), UserDto.class);
    }


}
