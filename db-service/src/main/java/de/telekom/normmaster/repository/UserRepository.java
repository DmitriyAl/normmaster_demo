package de.telekom.normmaster.repository;

import de.telekom.normmaster.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findById(Integer id);

    User findByLoginId(String loginId);
}
