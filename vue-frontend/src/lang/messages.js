import Vue from 'vue'
import VueI18n from 'vue-i18n'

import en from './en.json'
import de from './de.json'
import ru from './ru.json'

Vue.use(VueI18n);

const locale = localStorage.getItem("CURRENT_LOCALE");

const messages = {
    "en": en,
    "en-US": en,
    "de": de,
    "ru": ru,
    "ru-RU": ru
};

const i18n = new VueI18n({
    locale,
    messages
});

export default i18n