import 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import router from './router'
import i18n from './lang/messages'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'
import axios from 'axios'
import VueResource from 'vue-resource'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css'
import store from './store'
import './css/main.css'

Vue.use(VueResource)
Vue.use(BootstrapVue)

axios.defaults.withCredentials = true
// Setting up Axios on Vue Instance, for use via this.$axios
Vue.prototype.$axios = axios

Vue.config.productionTip = false;

Vue.use(Vuetify, {
  theme: {
    primary: colors.pink.darken1,
    secondary: '#b0bec5',
    accent: '#ff00ff',
    error: '#b71c1c'
  }
});

axios.interceptors.response.use(response => {
    return Promise.resolve(response)
  },
  error => {
    if (error.response.status === 401) {
      console.log('Unauthorized, logging out ...');
      store.dispatch('userSignOut')
      router.replace('login')
      return Promise.reject(error)
    } else {
      return Promise.reject(error.response);
    }
  })

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app');
