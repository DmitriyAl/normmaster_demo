import Vue from 'vue'
import Router from 'vue-router'
import Demo from './components/demo';
import Pdf from './components/pdf';
import Excel from './components/excel';
import Login from './components/login';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/demo',
      name: Demo,
      component: Demo
    },
    {
      path: '/pdf',
      name: Pdf,
      component: Pdf
    },
    {
      path: '/excel',
      name: Excel,
      component: Excel
    },
    {
      path: '/login',
      name: Login,
      component: Login,
      meta: {loginPage: true, nonRequiresAuth: true}
    }
  ]
});


router.beforeEach((to, from, next) => {
  const requiresAuth = !to.matched.some(record => record.meta.nonRequiresAuth)
  const isLoginPage = to.matched.some(record => record.meta.loginPage)
  const isAuthenticated = localStorage.getItem("auth")

  // alert(requiresAuth + " " + isLoginPage + " " + isAuthenticated)

   if (requiresAuth && !isAuthenticated) {
     next('/login')
   } else if (isLoginPage && isAuthenticated) {
     router.push('/demo')
   } else {
    next()
  }
})

export default router;
