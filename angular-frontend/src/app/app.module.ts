import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {MainLayoutComponent} from './main-layout/main-layout.component';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {FooterComponent} from './footer/footer.component';
import {PdfPageComponent} from './pdf-page/pdf-page.component';
import {ExcelPageComponent} from './excel-page/excel-page.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {PdfJsViewerModule} from 'ng2-pdfjs-viewer'
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import {LoginPageComponent} from './login-page/login-page.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ErrorInterceptor, fakeBackendProvider, JwtInterceptor} from "./_helpers";
import { IgxExcelModule } from "igniteui-angular-excel/ES5/igx-excel-module";
import { IgxSpreadsheetModule } from "igniteui-angular-spreadsheet/ES5/igx-spreadsheet-module";
import { CamundaPageComponent } from './camunda-page/camunda-page.component';
import { FirstStepComponent } from './camunda-page/first-step/first-step.component';
import { SecondStepComponent } from './camunda-page/second-step/second-step.component';
import { ThirdStepComponent } from './camunda-page/third-step/third-step.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule, MatStepperModule} from "@angular/material";
import { ArchwizardModule } from 'angular-archwizard';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MainLayoutComponent,
    MainMenuComponent,
    FooterComponent,
    PdfPageComponent,
    ExcelPageComponent,
    LoginPageComponent,
    CamundaPageComponent,
    FirstStepComponent,
    SecondStepComponent,
    ThirdStepComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PdfJsViewerModule,
    NgxExtendedPdfViewerModule,
    IgxExcelModule,
    IgxSpreadsheetModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatStepperModule,
    MatInputModule,
    ArchwizardModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
