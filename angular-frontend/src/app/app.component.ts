import {Component, OnInit} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'angular-frontend';

  constructor(private translateService: TranslateService) {
    translateService.setDefaultLang('en');
  }

  ngOnInit(): void {
  }
}
