import { TestBed } from '@angular/core/testing';

import { StepRedirectionService } from './step-redirection.service';

describe('StepRedirectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepRedirectionService = TestBed.get(StepRedirectionService);
    expect(service).toBeTruthy();
  });
});
