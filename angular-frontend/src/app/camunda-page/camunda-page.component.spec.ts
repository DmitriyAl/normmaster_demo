import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamundaPageComponent } from './camunda-page.component';

describe('CamundaPageComponent', () => {
  let component: CamundaPageComponent;
  let fixture: ComponentFixture<CamundaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamundaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamundaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
