import {Component, OnInit} from '@angular/core';
import {StepRedirectionService} from "../step-redirection.service";

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.less']
})
export class FirstStepComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }
}
