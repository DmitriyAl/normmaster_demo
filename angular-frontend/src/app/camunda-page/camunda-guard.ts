import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {StepRedirectionService} from "./step-redirection.service";

@Injectable({providedIn: 'root'})
export class CamundaGuard implements CanActivate {
  constructor(private redirectionService: StepRedirectionService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.redirectionService.goNext(state.url);
  }

}
