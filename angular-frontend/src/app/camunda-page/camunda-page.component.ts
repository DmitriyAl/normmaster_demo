import {Component, OnInit} from '@angular/core';
import {StepRedirectionService} from "./step-redirection.service";
import {MovingDirection} from "angular-archwizard";


@Component({
  selector: 'app-camunda-page',
  templateUrl: './camunda-page.component.html',
  styleUrls: ['./camunda-page.component.less']
})
export class CamundaPageComponent implements OnInit {

  constructor(private stepRedirectionService: StepRedirectionService) {
  }

  ngOnInit() {
  }

  secondStep: (direction: MovingDirection) => Promise<boolean> = (direction: MovingDirection) => {
    if (MovingDirection.Forwards == direction) {
      return this.stepRedirectionService.goSecondStep();
    } else {
      return Promise.resolve<boolean>(true);
    }
  };

  thirdStep: (direction: MovingDirection) => Promise<boolean> = (direction: MovingDirection) => {
    if (MovingDirection.Forwards == direction) {
      return this.stepRedirectionService.goThirdStep();
    } else {
      return Promise.resolve<boolean>(true);
    }
  };

  finishFunction() {

  }
}
