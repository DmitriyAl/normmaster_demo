import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StepRedirectionService {

  constructor(private http: HttpClient) { }

  goNext(url: string): Observable<boolean> {
    return this.http.get<boolean>(`${environment.apiUrl + url}`);
  }

  goSecondStep(): Promise<boolean> {
    return this.http.get<boolean>(`${environment.secondStep}`).toPromise()
  }

  goThirdStep(): Promise<boolean> {
    return this.http.get<boolean>(`${environment.thirdStep}`).toPromise()
  }
}
