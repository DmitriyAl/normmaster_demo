import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Credentials} from "./model/credentials";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private _authenticated: boolean = false;

  constructor(private http: HttpClient) {
  }

  authenticate(credentials: Credentials, callback) {
    const headers = new HttpHeaders(credentials ? {authorization: 'Basic ' + btoa(credentials.username + ':' + credentials.password)} : {});
    this.http.get('http://localhost:8082/', {headers: headers}).subscribe(response => {
      this._authenticated = !!response['name'];
      return callback && callback();
    })
  }

  get authenticated(): boolean {
    return this._authenticated;
  }
}
