import {Component, OnInit} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {Router} from "@angular/router";
import {AuthenticationService} from "../authentication/authentication.service";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.less']
})
export class NavBarComponent implements OnInit {

  constructor(private translateService: TranslateService, private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  useLanguage(lang: string) {
    this.translateService.use(lang);
  }

  logout() {
    this.authenticationService.logOut();
    this.router.navigateByUrl("/login");
  }
}
