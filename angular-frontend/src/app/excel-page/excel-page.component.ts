import {Component, OnInit, ViewChild} from '@angular/core';
import {IgxSpreadsheetComponent} from "igniteui-angular-spreadsheet/ES5/igx-spreadsheet-component";
import {ExcelUtility} from "../utilities/excel-utility";

@Component({
  selector: 'app-excel-page',
  templateUrl: './excel-page.component.html',
  styleUrls: ['./excel-page.component.less']
})
export class ExcelPageComponent implements OnInit {
  @ViewChild("spreadsheet", {static: false, read: IgxSpreadsheetComponent })
  public spreadsheet: IgxSpreadsheetComponent;

  constructor() { }

  public ngOnInit() {
    const excelFile = "assets/JScomparison.xlsx";
    ExcelUtility.loadFromUrl(excelFile).then((w) => {
      this.spreadsheet.workbook = w;
    });
  }
}
