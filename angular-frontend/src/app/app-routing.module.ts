import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PdfPageComponent} from "./pdf-page/pdf-page.component";
import {ExcelPageComponent} from "./excel-page/excel-page.component";
import {LoginPageComponent} from "./login-page/login-page.component";
import {AuthGuard} from "./_helpers";
import {CamundaPageComponent} from "./camunda-page/camunda-page.component";
import {FirstStepComponent} from "./camunda-page/first-step/first-step.component";
import {CamundaGuard} from "./camunda-page/camunda-guard";
import {SecondStepComponent} from "./camunda-page/second-step/second-step.component";
import {ThirdStepComponent} from "./camunda-page/third-step/third-step.component";

const routes: Routes = [
  {path: '', component: PdfPageComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginPageComponent},
  {path: 'pdf', component: PdfPageComponent, canActivate: [AuthGuard]},
  {path: 'excel', component: ExcelPageComponent, canActivate: [AuthGuard]},
  {
    path: 'camunda', component: CamundaPageComponent, canActivate: [AuthGuard],
    children: [
      {path: '', component: FirstStepComponent, canActivate: [CamundaGuard]},
      {path: 'firstStep', component: FirstStepComponent, canActivate: [CamundaGuard]},
      {path: 'secondStep', component: SecondStepComponent, canActivate: [CamundaGuard]},
      {path: 'thirdStep', component: ThirdStepComponent, canActivate: [CamundaGuard]},
    ]
  },
  // otherwise redirect to home
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
