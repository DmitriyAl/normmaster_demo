package com.tsystems.demo.controller;

import com.tsystems.demo.service.CamundaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

@CrossOrigin
@RestController
public class CamundaController {

    @Autowired
    private CamundaService camundaService;

    private String proString;

    @GetMapping({"camunda", "/camunda/firstStep"})
    public boolean firstStepCompleted() {
        proString = camundaService.startProcess();
        return true;
    }

    @GetMapping("/camunda/secondStep")
    public boolean secondStepCompleted() throws URISyntaxException {
        camundaService.completeCamundaTask(proString);
        return true;
    }

    @GetMapping("/camunda/thirdStep")
    public boolean thirdStepCompleted() throws URISyntaxException  {
        camundaService.completeCamundaTask(proString);
        return false;
    }
}
