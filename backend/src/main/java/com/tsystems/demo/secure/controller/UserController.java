package com.tsystems.demo.secure.controller;

import com.tsystems.demo.service.UserService;
import de.telekom.normmaster.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * <pre>
 * $Id: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * $LastChangedDate: $
 * </pre>
 *
 * @author stpereve
 */
@CrossOrigin
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @PostMapping(value = "/authorize")
    public Principal getUser(Principal user) {
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return "test";
            }
        };
        return principal;
    }

    @GetMapping
    public UserDto getUsers() {
        return userService.getUsers();
    }

    @PostMapping(value = "/login")
    public ResponseEntity postData() {
        return ResponseEntity.ok("Data is saved");
    }

    @GetMapping("/api/public")
    public String getPublicMessage() {
        return "Hello from public API controller";
    }

    @GetMapping("/api/private")
    public String getPrivateMessage() {
        return "Hello from private API controller";
    }
}
