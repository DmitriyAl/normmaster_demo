package com.tsystems.demo.jwt;

import com.tsystems.demo.service.DBUserService;
import com.tsystems.demo.service.UserService;
import de.telekom.normmaster.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private DBUserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        // request for the User from DB
        UserDto user = userService.getUserByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("No such user with email '" + email + "'");
        }
        return new User(user.getEmailAddress(), user.getPassword(), new ArrayList<>());
    }
}
