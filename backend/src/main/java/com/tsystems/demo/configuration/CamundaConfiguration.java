package com.tsystems.demo.configuration;

//import org.camunda.bpm.client.ExternalTaskClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * <pre>
 * $Id: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * $LastChangedDate: $
 * </pre>
 *
 * @author stpereve
 */
@Configuration
public class CamundaConfiguration {
//
//    @Bean
//    public ExternalTaskClient getExternalTaskClient() {
//        return ExternalTaskClient.create().baseUrl("http://localhost:8080/engine-rest").build();
//    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
