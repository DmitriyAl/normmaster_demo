package com.tsystems.demo.service;

import de.telekom.normmaster.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public class DBUserService {
    public UserDto getUserByEmail(String mail) {
        if (mail.equals("test")) {
            UserDto userDto = new UserDto();
            userDto.setId(1);
            userDto.setLoginId("1");
            userDto.setEmailAddress("test");
            //the password is "password"
            userDto.setPassword("$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6");
            return userDto;
        }
        return null;
    }
}
