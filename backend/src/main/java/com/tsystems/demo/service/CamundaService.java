package com.tsystems.demo.service;

import org.apache.commons.net.util.Base64;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * $Id: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * $LastChangedDate: $
 * </pre>
 *
 * @author stpereve
 */
@Service
public class CamundaService {

    @Autowired
    private RestTemplate restTemplate;

//    @Autowired
//    private ExternalTaskClient client;

    private String camundaUrl = "http://localhost:8080/engine-rest/";

    private String tenant = "normmaster";

    private String username = "demo";

    private String password = "demo";

    /**
     * @return
     */
    public String startProcess() {
        StringBuilder action = new StringBuilder(camundaUrl);
        action.append("process-definition/key/NormMaster_Process");
        action.append("/tenant-id/").append(tenant).append("/start");

        try {
            Map<String, String> response = restTemplate
                    .postForObject(action.toString(), new HttpEntity<>(getHeaders(username, password)), Map.class,
                                   new Object[0]);
            return response.get("id");
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return null;
    }

    /**
     * @param processId
     * @return
     */
    public Object completeCamundaTask(String processId) throws URISyntaxException {

        URIBuilder findTasks = new URIBuilder(camundaUrl + "task");
        findTasks.addParameter("processInstanceId", processId);
        System.out.println(findTasks.build());

        List<Map> list = restTemplate.getForObject(findTasks.build(), List.class);

        String taskComplete = camundaUrl + "task/" + list.get(0).get("id") + "/complete";

        System.out.println("taskComplete: " + list.get(0).get("id"));

        return restTemplate.postForObject(taskComplete, new HttpEntity<>(getHeaders(username, password)), Object.class);
    }

    /**
     * @param username
     * @param password
     * @return
     */
    private static HttpHeaders getHeaders(String username, String password) {
        HttpHeaders headers = new HttpHeaders();
        if (username != null && password != null) {
            String auth = username + ":" + password;


            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")));
            String authHeader = "Basic " + new String(encodedAuth);
            headers.add("Authorization", authHeader);
        }

        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
        return headers;
    }
}
