package com.tsystems.demo.service;

import de.telekom.normmaster.dto.UserDto;
import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class UserService {

    @Autowired
    private RestTemplate restTemplate;

    public UserDto getUsers() {
        try {
            UserDto response = restTemplate.getForObject("http://localhost:8086/user/1", UserDto.class);
            return response;
        } catch (Exception exc) {
            exc.printStackTrace();
            return null;
        }
    }

    public UserDto getUserByName(String name) {
        try {
            UserDto response = restTemplate.getForObject("http://localhost:8086/user/login/" + name, UserDto.class);
            return response;
        } catch (Exception exc) {
            exc.printStackTrace();
            return null;
        }
    }

}
