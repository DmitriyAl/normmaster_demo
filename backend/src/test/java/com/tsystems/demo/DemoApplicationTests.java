package com.tsystems.demo;

import org.apache.commons.net.ftp.FTPClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.FileSystem;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    private FakeFtpServer fakeFtpServer;

    private static final int PORT = 9999;

    private static final String HOME_DIR = "/";
    private static final String FILE = "/dir/sample.txt";
    private static final String CONTENTS = "abcdef 1234567890";
    private static final String USERNAME = "user";
    private static final String PASSWORD = "password";

    @Before
    public void setUp() throws Exception {
        fakeFtpServer = new FakeFtpServer();
        fakeFtpServer.setServerControlPort(PORT); // use any free port

        FileSystem fileSystem = new UnixFakeFileSystem();
        fileSystem.add(new FileEntry(FILE, CONTENTS));
        fakeFtpServer.setFileSystem(fileSystem);

        UserAccount userAccount = new UserAccount(USERNAME, PASSWORD, HOME_DIR);
        fakeFtpServer.addUserAccount(userAccount);
        fakeFtpServer.start();

        int port = fakeFtpServer.getServerControlPort();
        port = fakeFtpServer.getServerControlPort();
    }


    @Test
    public void testFtpDownload() throws Exception {
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect("localhost", PORT);
        ftpClient.login(USERNAME, PASSWORD);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        boolean success = ftpClient.retrieveFile(FILE, outputStream);
        ftpClient.disconnect();

        if (!success) {
            throw new IOException("Retrieve file failed: " + FILE);
        }

        String u = outputStream.toString();
        outputStream.close();
    }

    @After
    public void tearDown() throws Exception {
        fakeFtpServer.stop();
    }
}
